/*
   Copyright 2019 Jan Dittberner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this program except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package services

import "net/http"

type StaffMember struct {
	Username string `json:"username"`
	RealName string `json:"realName"`
	Role     string `json:"role"`
}

type GetStaffMemberResponse struct {
	Members []StaffMember `json:"members"`
}

func GetStaffMembers(r *http.Request) (*GetStaffMemberResponse, error) {
	result := &GetStaffMemberResponse{}
	if err := apiClientInstance.Call(r, "GET", apiStaffMembers, nil, result); err != nil {
		return nil, err
	}
	return result, nil
}
