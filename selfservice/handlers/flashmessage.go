/*
   Copyright 2019 Jan Dittberner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this program except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package handlers

import (
	"context"
	"encoding/gob"
	"net/http"

	"github.com/gorilla/sessions"
	log "github.com/sirupsen/logrus"
)

type FlashSeverity string

const (
	FlashError   FlashSeverity = "error"
	FlashSuccess FlashSeverity = "success"
	FlashWarning FlashSeverity = "warning"
	FlashInfo    FlashSeverity = "info"
)

type FlashMessage struct {
	Header   string
	Severity FlashSeverity
	Message  string
}

var store *sessions.CookieStore

func SetupCookieStore(cookieSecret []byte) {
	store = sessions.NewCookieStore(cookieSecret)
	gob.Register(FlashMessage{})
}

const sessionCookieName = "cacert-session"

func handleFlashes(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		session, err := store.Get(r, sessionCookieName)
		if err != nil {
			internalError(w, err)
			return
		}
		flashes := session.Flashes()

		requestContext := context.WithValue(r.Context(), ctxFlashMessages, flashes)
		if err = session.Save(r, w); err != nil {
			internalError(w, err)
			return
		}

		next.ServeHTTP(w, r.WithContext(requestContext))

	})
}

func getFlashes(context context.Context) []interface{} {
	if flashes := context.Value(ctxFlashMessages); flashes != nil {
		return flashes.([]interface{})
	}
	return nil
}

type FlashMessageAction struct{}

func (a *FlashMessageAction) AddFlash(w http.ResponseWriter, r *http.Request, message *FlashMessage, tags ...string) {
	session, err := store.Get(r, sessionCookieName)
	if err != nil {
		log.Warnf("could not get session cookie: %v", err)
		return
	}
	session.AddFlash(message, tags...)
	err = session.Save(r, w)
	if err != nil {
		log.Warnf("could not save flash message: %v", err)
		return
	}
}
