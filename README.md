# CAcert community self service system

This project contains the source code for the CAcert community self service
system software.

## License

The CAcert community self service system software is licensed under the terms
of the Apache License, Version 2.0.

    Copyright 2019, 2020 Jan Dittberner

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this program except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

## History

The CAcert community self service system software is a [Go] reimplementation of
the ancient PHP implementation in `staff.php` and `password.php` that has been
running on community.cacert.org for many years.

### Architectural differences

The old implementation required direct database access to the account database
on [email.infra.cacert.org](https://infradocs.cacert.org/systems/email.html)
while the new software is split into a frontend part (this software) and a
separate backend API in [a different
repository](https://git.cacert.org/cacert-selfservice-api.git) that gates
access to specific parts of the database.

---

## Development requirements

Local development requires

* golang >= 1.14
* GNU make

On a Debian 10 (Buster) system with
[backports](https://backports.debian.org/Instructions/) enabled you can run the
following command to get all required dependencies:

```
sudo apt install make
sudo apt install -t buster-backports golang-go
```

## Getting started

Clone the code via git:

```shell script
git clone ssh://git.cacert.org/var/cache/git/cacert-selfservice.git
```

To get started you can build and run the application. This will generate valid
data for a `config.yaml` file and a fragment of client credentials that need to
be configured on the API server side that you will need to customize.

```shell script
make
./cacert-selfservice
```

You will also need a set of X.509 certificates and a private key because the
application performs TLS client certificate authentication. You might use
`openssl` to create a self signed server certificate and retrieve the CAcert
root certificates from the CAcert website:

```shell script
openssl req -new -newkey rsa:2048 -nodes -keyout server.key.pem -x509 -out server.crt.pem -subj '/CN=localhost'
( curl -s http://www.cacert.org/certs/class3_X0E.crt ; curl -s http://www.cacert.org/certs/root_X0F.crt ) > cacert_roots.pem
```

### Customize `config.yaml`

You can use the following table to find useful values for the parameters in
`config.yaml`.

Parameter | Description | How to get a valid value
----------|-------------|-------------------------
`client_ca_certificates` | PEM file containing accepted client certificate CAs | use `cacert_roots.pem` from above
`server_certificate` | Server certificate for TLS | use the `server.crt.pem` from above or a properly CA signed certificate for a real system
`server_key` | Server private key for TLS | use the `server.key.pem` from above
`cookie_secret` | Secret for encrypting cookies | use the generated value or see [Generating random byte values](#generating-random-byte-values) below
`csrf_key` | Secret seed value for CSRF tokens | use the generated value or see [Generating random byte values](#generating-random-byte-values) below
`base_url` | URL that will be used to build absolute URLs | configure to the real address of your instance
`https_address` | listening address for HTTPS | use `:443` for production, keep the generated value for a local setup
`admin_emails` | email addresses of administrators | list the email addresses that need to be part of administrators' client certificates
`api_private_key` | ECDSA private key for signing API requests | use the generated value and add the matching public key on the API server
`api_client_id` | Unique client identifier for API requests | use the generated value and add it on the API server too
`api_ca_certificates` | PEM file containing CA certificates that will be used to validate the API server TLS certificate | use a file that contains the CA certificates that signed you API server certificate
`api_endpoint_url` | HTTPS endpoint address were the API server is available | configure the real address of your API server instance
`access_log` | Path to the HTTP access log file | a file path. If this is not set access logs are written to the standard output
`jwt_private_key` | ECDSA private key for signing JWT tokens | use the generated value |
`jwt_validity_hours` | hours that the JWT tokens should be valid | should a reasonable but short value, the default of `24` should be ok |

### Generating random byte values

```shell script
dd if=/dev/urandom bs=32 count=1 2>/dev/null | base64
```

## Code structure

```
.
├── config.yaml.example
├── go.mod
├── go.sum
├── Jenkinsfile
├── LICENSE
├── Makefile
├── README.md
├── selfservice
│   ├── assets_fs.go
│   ├── generate_assets.go
│   ├── handlers
│   ├── main.go
│   ├── resources
│   │   ├── static
│   │   └── templates
│   ├── services
│   ├── templatemanager.go
│   └── types.go
└── selfservice.go
```

The `selfservice` directory contains the application code, static assets and
[Go templates] for HTML pages. The HTTP handlers are contained in the
`handlers` package, access to the API server is implemented in the `services`
package.
The entry point into the application is in the `selfservice.go` file in the top
level directory.

The HTML templates use [Semantic UI] and [jQuery] which are both embedded into
the resulting binary at build time.  `Makefile` controls the build.
`Jenkinsfile` contains the pipeline definition for the [Continuous Integration
Job].

[Continuous Integration Job]: https://jenkins.cacert.org/job/cacert-selfservice/
[Go]: https://golang.org/
[Go templates]: https://golang.org/pkg/text/template/
[jQuery]: https://jquery.com/
[Semantic UI]: https://semantic-ui.com/
