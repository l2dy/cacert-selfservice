/*
   Copyright 2019, 2020 Jan Dittberner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this program except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package handlers

import (
	"fmt"
	"net/http"
	"strings"

	log "github.com/sirupsen/logrus"

	"git.cacert.org/cacert-selfservice/selfservice"
	"git.cacert.org/cacert-selfservice/selfservice/services"
)

type AdminPasswordResetForm struct {
	Email          string
	NewPassword    string
	PasswordRepeat string
	Emails         []string
	Payload        *services.ResetPasswordPayload
	Errors         map[string]string
	Remarks        string
}

func (f *AdminPasswordResetForm) Validate() (bool, *services.ResetPasswordPayload) {
	f.Errors = make(map[string]string)

	data := f.Payload

	data.IsAdmin = true

	emailAddress := strings.TrimSpace(f.Email)
	for _, e := range f.Emails {
		if e == emailAddress && strings.HasSuffix(emailAddress, "@cacert.org") {
			data.EmailAddress = emailAddress
		}
	}
	if data.EmailAddress == "" {
		f.Errors[fieldEmail] = "You need to choose one of your own cacert.org email addresses"
	}

	newPassword := strings.TrimSpace(f.NewPassword)
	newPasswordRepeat := strings.TrimSpace(f.PasswordRepeat)

	if len(newPassword) < MinNewPasswordLength {
		f.Errors[fieldNewPassword] = fmt.Sprintf("Too short (need at least %d characters)", MinNewPasswordLength)
	} else if len(newPassword) > MaxPasswordLength {
		f.Errors[fieldNewPassword] = fmt.Sprintf("Too long (maximum of %d characters allowed)", MaxPasswordLength)
	}

	if newPassword != newPasswordRepeat {
		f.Errors[fieldPasswordRepeat] = "passwords do not match"
	}
	data.NewPassword = newPassword

	remarks := strings.TrimSpace(f.Remarks)
	if remarks == "" {
		f.Errors[fieldRemarks] = "Please specify the reason for the email account creation"
	} else if len(remarks) < MinRemarksLength || len(remarks) > MaxRemarksLength {
		f.Errors[fieldRemarks] = fmt.Sprintf("Reason must consist of %d to %d characters", MinRemarksLength, MaxRemarksLength)
	}
	data.Remarks = remarks

	return len(f.Errors) == 0, data
}

type adminPasswordResetHandler struct {
	FlashMessageAction
}

func (h *adminPasswordResetHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var templateContext struct {
		Form *AdminPasswordResetForm
		baseTemplateContext
	}

	switch r.Method {
	case http.MethodPost:
		emails, err := h.fetchEmailAddresses(w, r)
		if err != nil {
			return
		}

		form := &AdminPasswordResetForm{
			Emails:         emails,
			Email:          r.FormValue(fieldEmail),
			NewPassword:    r.FormValue(fieldNewPassword),
			PasswordRepeat: r.FormValue(fieldPasswordRepeat),
			Remarks:        r.FormValue(fieldRemarks),
			Payload:        &services.ResetPasswordPayload{},
		}

		if valid, data := form.Validate(); !valid {
			templateContext.SetupContext(r)
			templateContext.Form = form
		} else {
			data.RequesterEmailAddress = getEmails(r.Context())[0]
			response, err := services.ResetPassword(r, data)
			if err != nil {
				log.Errorf("error calling reset password API: %v", err)
				h.AddFlash(w, r, &FlashMessage{
					Header:   "Could not process request",
					Severity: FlashError,
					Message:  "We could not process the password reset request. Try again later.",
				})
			} else if response.Success {
				log.Infof(
					"Password reset for user %s with email %s successful.",
					response.Username, data.EmailAddress)
				h.AddFlash(w, r, &FlashMessage{
					Header:   "Success",
					Severity: FlashSuccess,
					Message:  response.Message,
				})
			} else {
				log.Infof(
					"Password reset for email %s failed.", data.EmailAddress)
				h.AddFlash(w, r, &FlashMessage{
					Severity: FlashError,
					Message:  response.Message,
				})
			}
			http.Redirect(w, r, UrlAdminPasswordReset, http.StatusFound)
			return
		}
		break
	case http.MethodGet:
		templateContext.SetupContext(r)

		emails, err := h.fetchEmailAddresses(w, r)
		if err != nil {
			return
		}

		templateContext.Form = &AdminPasswordResetForm{
			Emails: emails,
		}

		break
	}
	if err := selfservice.RenderTemplate(w, "admin-password-reset.tmpl.html", http.StatusOK, templateContext); err != nil {
		internalError(w, err)
		return
	}
}

func (h *adminPasswordResetHandler) fetchEmailAddresses(w http.ResponseWriter, r *http.Request) ([]string, error) {
	emailAddressResponse, err := services.GetEmailAddresses(r, "")
	if err != nil {
		log.Errorf("error calling get email addresses API: %v", err)
		h.AddFlash(w, r, &FlashMessage{
			Header:   "Could not process request",
			Severity: FlashError,
			Message:  "We could not check whether the request is valid. Try again later.",
		})
		http.Redirect(w, r, UrlErrorPage, http.StatusFound)
		return nil, err
	}
	emails := emailAddressResponse.EmailAddresses
	return emails, nil
}
