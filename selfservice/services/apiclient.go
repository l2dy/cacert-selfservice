/*
   Copyright 2019 Jan Dittberner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this program except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package services

import (
	"bytes"
	"crypto/ecdsa"
	"crypto/rand"
	"crypto/sha256"
	"crypto/tls"
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"

	log "github.com/sirupsen/logrus"
)

const (
	apiAliases         = "/aliases"
	apiEmailAccount    = "/email-account"
	apiEmailAddresses  = "/email-addresses"
	apiEmailPassword   = "/email-password"
	apiInitialPassword = "/initial-password"
	apiStaffMembers    = "/staff-members"
)

type apiClient struct {
	privateKey *ecdsa.PrivateKey
	endpoint   string
	clientId   string
	httpClient *http.Client
}

var apiClientInstance *apiClient

func ConfigureAPIClient(
	clientId string, privateKey *ecdsa.PrivateKey, endpoint string, apiCAs string) (err error) {
	rootCAs := x509.NewCertPool()
	pemData, err := ioutil.ReadFile(apiCAs)
	if err != nil {
		return fmt.Errorf("could not load API CA certificates: %v", err)
	}
	rootCAs.AppendCertsFromPEM(pemData)
	tlsConfig := &tls.Config{RootCAs: rootCAs, MinVersion: tls.VersionTLS12}
	trTls12 := &http.Transport{
		TLSClientConfig:     tlsConfig,
		MaxIdleConnsPerHost: 2,
	}
	client := &http.Client{
		Transport: trTls12,
		Timeout:   time.Duration(10 * time.Second),
	}

	apiClientInstance = &apiClient{
		clientId:   clientId,
		privateKey: privateKey,
		endpoint:   endpoint,
		httpClient: client,
	}
	return
}

func (c *apiClient) Call(originalRequest *http.Request, method string, path string, payload interface{}, result interface{}) error {
	requestUrl, err := url.Parse(c.endpoint)
	if err != nil {
		return fmt.Errorf("error parsing base URL: %v", err)
	}
	requestUrl, err = requestUrl.Parse(path)
	if err != nil {
		return fmt.Errorf("error parsing request URL: %v", err)
	}

	sha256Hash := sha256.New()
	sha256Hash.Write([]byte(path))

	var response *http.Response
	var request *http.Request

	if payload != nil {
		payloadBytes, err := json.Marshal(payload)
		if err != nil {
			return fmt.Errorf("error marshaling payload to json: %v", err)
		}
		sha256Hash.Write(payloadBytes)
		request, err = http.NewRequest(method, requestUrl.String(), bytes.NewReader(payloadBytes))
	} else {
		request, err = http.NewRequest(method, requestUrl.String(), nil)
	}
	if err != nil {
		return fmt.Errorf("could not create request: %v", err)
	}

	signatureBytes, err := c.privateKey.Sign(rand.Reader, sha256Hash.Sum(make([]byte, 0)), nil)
	if err != nil {
		return fmt.Errorf("error signing request data with private key: %v", err)
	}

	request.Header.Set("Client-Id", c.clientId)
	request.Header.Set("Hash-Signature", base64.RawStdEncoding.EncodeToString(signatureBytes))
	request.Header.Set("Original-Addr", originalRequest.RemoteAddr)
	response, err = c.httpClient.Do(request)
	if err != nil {
		return fmt.Errorf("error performing request: %v", err)
	}

	defer func() { _ = response.Body.Close() }()

	if response.StatusCode >= 500 {
		log.Errorf("request failed: %s %s -> %s",
			request.Method, request.URL, response.Status)
		return fmt.Errorf("API request failed because of server")
	}

	buf := new(bytes.Buffer)
	_, err = io.Copy(buf, response.Body)

	if err = json.Unmarshal(buf.Bytes(), result); err != nil {
		return fmt.Errorf("could not parse response: %v", err)
	}

	if response.StatusCode >= 400 {
		log.Errorf("request failed: %s %s -> %s",
			request.Method, request.URL, response.Status)
		return fmt.Errorf("API request failed because of client")
	}

	return nil
}
