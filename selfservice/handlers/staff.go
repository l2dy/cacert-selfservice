/*
   Copyright 2019 Jan Dittberner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this program except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package handlers

import (
	"net/http"

	log "github.com/sirupsen/logrus"

	"git.cacert.org/cacert-selfservice/selfservice"
	"git.cacert.org/cacert-selfservice/selfservice/services"
)

type staffHandler struct {
	FlashMessageAction
}

func (h *staffHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var templateContext struct {
		Aliases      []services.Alias
		StaffMembers []services.StaffMember
		baseTemplateContext
	}
	templateContext.SetupContext(r)
	staff, err := services.GetStaffMembers(r)
	if err != nil {
		log.Errorf("error calling get staff members API: %v", err)
		h.AddFlash(w, r, &FlashMessage{
			Header:   "Could not process request",
			Severity: FlashError,
			Message:  "We could not check whether the request is valid. Try again later.",
		})
		http.Redirect(w, r, "/error-page", http.StatusFound)
		return
	}
	templateContext.StaffMembers = staff.Members

	aliases, err := services.GetAllAliases(r)
	if err != nil {
		log.Errorf("error calling get aliases API: %v", err)
		h.AddFlash(w, r, &FlashMessage{
			Header:   "Could not process request",
			Severity: FlashError,
			Message:  "We could not check whether the request is valid. Try again later.",
		})
		http.Redirect(w, r, "/error-page", http.StatusFound)
		return
	}
	templateContext.Aliases = aliases.Aliases

	if err := selfservice.RenderTemplate(w, "staff.tmpl.html", http.StatusOK, templateContext); err != nil {
		internalError(w, err)
		return
	}
}
