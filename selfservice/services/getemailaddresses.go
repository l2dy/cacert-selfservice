/*
   Copyright 2019 Jan Dittberner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this program except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package services

import (
	"fmt"
	"net/http"
	"net/url"
)

type GetEmailAddressesResponse struct {
	EmailAddresses []string `json:"emailAddresses"`
}

func GetEmailAddresses(r *http.Request, filterSuffix string) (result *GetEmailAddressesResponse, err error) {
	result = &GetEmailAddressesResponse{}
	err = apiClientInstance.Call(r,
		"GET", fmt.Sprintf("%s?filter=%s", apiEmailAddresses, url.QueryEscape(filterSuffix)), nil, result,
	)
	return
}
