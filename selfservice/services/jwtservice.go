/*
   Copyright 2020 Jan Dittberner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this program except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package services

import (
	"crypto/ecdsa"
	"fmt"
	"strings"

	"github.com/dgrijalva/jwt-go"
)

type jwtService struct {
	privateKey *ecdsa.PrivateKey
}

var jwtServiceInstance *jwtService

func ConfigureJwtService(key *ecdsa.PrivateKey) {
	jwtServiceInstance = &jwtService{privateKey: key}
}

type InitialRequestToken struct {
	EmailAddress string
	jwt.StandardClaims
}

func BuildTokenFromString(tokenString string) (*InitialRequestToken, error) {
	claims := &InitialRequestToken{}
	token, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		return jwtServiceInstance.privateKey.Public(), nil
	})
	if err != nil {
		return nil, err
	}

	if claims, ok := token.Claims.(*InitialRequestToken); ok && token.Valid {
		fmt.Printf("%v %v", claims.Subject, claims.IssuedAt)
	}
	return claims, err
}

func (t *InitialRequestToken) Serialize() (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodES256, t)
	return token.SignedString(jwtServiceInstance.privateKey)
}

func (t *InitialRequestToken) CheckEmailMatch(emails []string) error {
	for _, e := range emails {
		if e == strings.ToLower(t.EmailAddress) {
			return nil
		}
	}
	return fmt.Errorf(
		"client certificate for email address(es) %s is not valid for the email address %s the request token has"+
			" been issued for", strings.Join(emails, ", "), t.EmailAddress)
}
