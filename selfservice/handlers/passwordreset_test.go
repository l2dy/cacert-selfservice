package handlers

import (
	"context"
	"testing"

	"git.cacert.org/cacert-selfservice/selfservice/services"
)

func TestPasswordResetForm_Validate_Happy(t *testing.T) {
	tctx := context.WithValue(context.Background(), ctxEmails, []string{"test@example.org", "test@cacert.org"})

	form := &PasswordResetForm{
		Email:          "test@cacert.org",
		NewPassword:    "a password",
		PasswordRepeat: "a password",
		Payload:        &services.ResetPasswordPayload{},
	}

	ok, result := form.Validate(getEmails(tctx))
	if !ok {
		t.Errorf("not ok: errors: %v, result: %v", form.Errors, result)
	}
}

func TestPasswordResetForm_Validate_NotMatching(t *testing.T) {
	tctx := context.WithValue(context.Background(), ctxEmails, []string{"test@example.org"})

	form := &PasswordResetForm{
		Email:          "test@cacert.org",
		NewPassword:    "a password",
		PasswordRepeat: "a password",
		Payload:        &services.ResetPasswordPayload{},
	}

	ok, result := form.Validate(getEmails(tctx))
	if ok {
		t.Errorf("unexpected ok: errors: %v, result: %v", form.Errors, result)
	}
}

func TestPasswordResetForm_Validate_Other_Address(t *testing.T) {
	tctx := context.WithValue(context.Background(), ctxEmails, []string{"test@example.org", "test@cacert.org"})

	form := &PasswordResetForm{
		Email:          "test_abc@cacert.org",
		NewPassword:    "a password",
		PasswordRepeat: "a password",
		Payload:        &services.ResetPasswordPayload{},
	}

	ok, result := form.Validate(getEmails(tctx))
	if ok {
		t.Errorf("unexpected ok: errors: %v, result: %v", form.Errors, result)
	}
}
