VERSION := $(shell git describe --always --dirty=-dev)
BUILD := $(shell date --iso-8601=seconds --utc)
GOFILES = $(shell find . -type f -name '*.go')
RESOURCES = $(shell find selfservice/resources/ -type f)
RESOURCE_VFS_FILE = selfservice/assets_vfsdata.go

all: cacert-selfservice

${RESOURCE_VFS_FILE}: ${RESOURCES} selfservice/main.go
	go generate -v ./...

cacert-selfservice: ${GOFILES} ${RESOURCE_VFS_FILE}
	go build -o $@ -x -ldflags " -X 'main.version=${VERSION}' -X 'main.build=${BUILD}'"

clean:
	rm -f cacert-selfservice

distclean: clean
	rm -f ${RESOURCE_VFS_FILE}

.PHONY: clean distclean all
