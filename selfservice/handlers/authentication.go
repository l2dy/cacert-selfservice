/*
   Copyright 2019 Jan Dittberner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this program except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package handlers

import (
	"context"
	"crypto/x509"
	"net/http"
	"strings"

	"git.cacert.org/cacert-selfservice/selfservice"
)

type contextKey int

const (
	ctxAuthenticatedCert contextKey = iota
	ctxIsAdmin
	ctxFlashMessages
	ctxEmails
)

type AuthInfo struct {
	NeedsAuthentication bool
	NeedsAdmin          bool
	AdminEmails         []string
}

func AdminAuth(adminEmails []string) *AuthInfo {
	return &AuthInfo{NeedsAdmin: true, NeedsAuthentication: true, AdminEmails: adminEmails}
}

func AnonymousAuth(adminEmails []string) *AuthInfo {
	return &AuthInfo{AdminEmails: adminEmails}
}

func UserAuth(adminEmails []string) *AuthInfo {
	return &AuthInfo{NeedsAuthentication: true, AdminEmails: adminEmails}
}

func authenticateRequest(authInfo *AuthInfo, next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var isAuthenticated, isAdmin bool
		var requestContext = r.Context()

		if len(r.TLS.PeerCertificates) > 0 {
			cert := r.TLS.PeerCertificates[0]
			for _, extKeyUsage := range cert.ExtKeyUsage {
				if extKeyUsage == x509.ExtKeyUsageClientAuth {
					emails := make([]string, 0)
					for _, emailAddress := range cert.EmailAddresses {
						isAuthenticated = true
						emails = append(emails, strings.ToLower(emailAddress))
					}
					for _, emailAddress := range emails {
						for _, adminAddress := range authInfo.AdminEmails {
							if adminAddress == emailAddress {
								isAdmin = true
								break
							}
						}
						if isAdmin {
							break
						}
					}
					requestContext = context.WithValue(requestContext, ctxIsAdmin, isAdmin)
					requestContext = context.WithValue(requestContext, ctxEmails, emails)
					requestContext = context.WithValue(requestContext, ctxAuthenticatedCert, cert)
				}
			}
		}

		if (authInfo.NeedsAuthentication && !isAuthenticated) || (authInfo.NeedsAdmin && !isAdmin) {
			var templateContext struct {
				baseTemplateContext
			}
			templateContext.SetupContext(r.WithContext(requestContext))
			if err := selfservice.RenderErrorTemplate(w, http.StatusForbidden, templateContext); err != nil {
				internalError(w, err)
			}
			return
		}

		next.ServeHTTP(w, r.WithContext(requestContext))
	})
}

func hasCAcertEmail(emails []string) bool {
	for _, email := range emails {
		if strings.HasSuffix(email, "@cacert.org") {
			return true
		}
	}
	return false
}

func getCertificate(c context.Context) *x509.Certificate {
	if authenticatedCertificate, ok := c.Value(ctxAuthenticatedCert).(*x509.Certificate); ok {
		return authenticatedCertificate
	}
	return nil
}

func getEmails(c context.Context) []string {
	if authenticatedEmails, ok := c.Value(ctxEmails).([]string); ok {
		return authenticatedEmails
	}
	return nil
}

func isAdmin(ctx context.Context) bool {
	value, ok := ctx.Value(ctxIsAdmin).(bool)
	return ok && value
}
