/*
   Copyright 2019 Jan Dittberner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this program except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package selfservice

import (
	"fmt"
	"html/template"
	"io/ioutil"
	"math/big"
	"net/http"
	"path/filepath"
	"strings"

	"github.com/Masterminds/sprig"
	"github.com/gorilla/csrf"
	"github.com/shurcooL/httpfs/path/vfspath"
	log "github.com/sirupsen/logrus"
	"github.com/valyala/bytebufferpool"
)

var templates map[string]*template.Template
var mainTmpl = `{{define "main" }} {{ template "base" . }} {{ end }}`

type TemplateConfig struct {
	TemplateLayoutPath  string
	TemplateIncludePath string
}

type TemplateError struct {
	s string
}

func (e *TemplateError) Error() string {
	return e.s
}

func NewError(text string) error {
	return &TemplateError{text}
}

var templateConfig *TemplateConfig

func SetTemplateConfig(layoutPath, includePath string) {
	templateConfig = &TemplateConfig{layoutPath, includePath}
}

func LoadTemplates() (err error) {
	if templateConfig == nil {
		err = NewError("TemplateConfig not initialized")
	}
	if templates == nil {
		templates = make(map[string]*template.Template)
	}

	layoutFiles, err := vfspath.Glob(Assets, filepath.Join(templateConfig.TemplateLayoutPath, "*.tmpl.*"))
	if err != nil {
		return err
	}

	includeFiles, err := vfspath.Glob(Assets, filepath.Join(templateConfig.TemplateIncludePath, "*.tmpl.*"))
	if err != nil {
		return err
	}

	mainTemplate := template.New("main")

	mainTemplate, err = mainTemplate.Parse(mainTmpl)
	if err != nil {
		log.Fatal(err)
	}

	funcMaps := sprig.FuncMap()
	funcMaps["nl2br"] = func(text string) template.HTML {
		return template.HTML(strings.Replace(template.HTMLEscapeString(text), "\n", "<br>", -1))
	}
	funcMaps[csrf.TemplateTag] = func(r *http.Request) template.HTML {
		return csrf.TemplateField(r)
	}
	funcMaps["hex"] = func(number *big.Int) template.HTML {
		return template.HTML(fmt.Sprintf("%x", number))
	}

	for _, file := range includeFiles {
		fileName := filepath.Base(file)
		files := append(layoutFiles, file)
		templates[fileName], err = mainTemplate.Clone()
		for _, t := range files {
			asset, err := Assets.Open(t)
			if err != nil {
				return err
			}
			if assetBytes, err := ioutil.ReadAll(asset); err != nil {
				return fmt.Errorf("error loading template %s from assets: %v", t, err)
			} else {
				if _, err := templates[fileName].New(t).Funcs(funcMaps).Parse(string(assetBytes)); err != nil {
					return fmt.Errorf("error parsing template %s: %v", t, err)
				}
			}
		}
	}

	log.Info("templates loading successful")
	return nil
}

func RenderErrorTemplate(w http.ResponseWriter, status int, data interface{}) error {
	name := fmt.Sprintf("%d.tmpl.html", status)

	_, ok := templates[name]
	if !ok {
		http.Error(w, http.StatusText(status), status)
		return nil
	}

	return RenderTemplate(w, name, status, data)
}

func RenderTemplate(w http.ResponseWriter, name string, status int, data interface{}) error {
	tmpl, ok := templates[name]
	if !ok {
		err := NewError(fmt.Sprintf("Template %s doesn't exist", name))
		return err
	}

	buf := bytebufferpool.Get()
	defer bytebufferpool.Put(buf)

	err := tmpl.Execute(buf, data)
	if err != nil {
		err := NewError(fmt.Sprintf("Template execution failed: %v", err))
		return err
	}

	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.WriteHeader(status)

	_, _ = buf.WriteTo(w)
	return nil
}
