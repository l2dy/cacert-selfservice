/*
   Copyright 2020 Jan Dittberner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this program except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package handlers

import (
	"fmt"
	"net/http"
	"net/mail"
	"net/url"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	log "github.com/sirupsen/logrus"

	"git.cacert.org/cacert-selfservice/selfservice"
	"git.cacert.org/cacert-selfservice/selfservice/services"
)

type CreateEmailAccountForm struct {
	RealName       string
	ContactEmail   string
	UserName       string
	FullNameAlias  string
	FullNameAlias2 string
	Payload        *services.CreateEmailAccountPayload
	Errors         map[string]string
	Role           string
	Remarks        string
}

func (f *CreateEmailAccountForm) Validate() (bool, *services.CreateEmailAccountPayload) {
	f.Errors = make(map[string]string)

	data := f.Payload

	realName := strings.ToValidUTF8(strings.TrimSpace(f.RealName), "")
	if realName == "" {
		f.Errors[fieldRealName] = "Please specify the real name of the email account user"
	} else if len(realName) < MinRealNameLength || len(realName) > MaxRealNameLength {
		f.Errors[fieldRealName] = fmt.Sprintf("Real name must consist of %d to %d characters", MinRealNameLength, MaxRealNameLength)
	}
	data.RealName = realName

	contactEmail := strings.TrimSpace(f.ContactEmail)
	if contactEmail == "" {
		f.Errors[fieldContactEmail] = "Please specify the contact email address of the email account user"
	} else if address, err := mail.ParseAddress(contactEmail); err != nil {
		f.Errors[fieldContactEmail] = "Please specify a valid email address"
	} else if strings.HasSuffix(address.Address, "@cacert.org") {
		f.Errors[fieldContactEmail] = "The contact email address must not be a cacert.org email address"
	}
	data.ContactEmail = contactEmail

	data.ContactEmail = contactEmail

	userName := strings.ToLower(strings.TrimSpace(f.UserName))
	if userName == "" {
		f.Errors[fieldUserName] = "Please specify the user name for the email account"
	} else if len(userName) < MinUserNameLength || len(userName) > MaxUserNameLength {
		f.Errors[fieldUserName] = fmt.Sprintf("User name must consist of %d to %d ASCII letters", MinUserNameLength, MaxUserNameLength)
	} else if !userNameRegexp.MatchString(userName) {
		f.Errors[fieldUserName] = fmt.Sprintf("Lowercase ASCII letters are allowed in user names exclusively")
	}
	data.UserName = userName

	fullNameAlias := strings.ToLower(strings.TrimSpace(f.FullNameAlias))
	if fullNameAlias == "" {
		f.Errors[fieldFullNameAlias] = "Please specify the full name alias of the email account user"
	} else if len(fullNameAlias) < MinUserNameLength || len(fullNameAlias) > MaxFullNameAliasLength {
		f.Errors[fieldFullNameAlias] = fmt.Sprintf("Full name alias must consist of %d to %d ASCII letters and may contain dots", MinUserNameLength, MaxFullNameAliasLength)
	} else if !fullNameRegexp.MatchString(fullNameAlias) {
		f.Errors[fieldFullNameAlias] = "Full name alias must consist of ascii letters and dots only and must start with a letter"
	} else if fullNameAlias == userName {
		f.Errors[fieldFullNameAlias] = "Full name alias must not be the same as the user name"
	}
	data.FullNameAlias = fullNameAlias

	fullNameAlias2 := strings.ToLower(strings.TrimSpace(f.FullNameAlias2))
	if fullNameAlias2 != "" {
		if len(fullNameAlias2) < MinUserNameLength || len(fullNameAlias2) > MaxFullNameAlias2Length {
			f.Errors[fieldFullNameAlias2] = fmt.Sprintf(
				"If the second full name alias is not empty it must consist of %d to %d ASCII letters and may contain dots", MinUserNameLength, MaxFullNameAliasLength)
		} else if !fullNameRegexp.MatchString(fullNameAlias2) {
			f.Errors[fieldFullNameAlias2] = "Second full name alias must consist of ascii letters and dots only and must start with a letter"
		} else if fullNameAlias2 == userName || fullNameAlias2 == fullNameAlias {
			f.Errors[fieldFullNameAlias2] = "Second full name alias must not be the same as the user name or the first full name alias"
		}
	}
	data.FullNameAlias2 = fullNameAlias2

	role := strings.TrimSpace(f.Role)
	if role != "" {
		if len(role) > MaxRoleLength {
			f.Errors[fieldRole] = fmt.Sprintf(
				"Role must not be longer than %d characters", MaxRoleLength)
		}
	}
	data.Role = role

	remarks := strings.TrimSpace(f.Remarks)
	if remarks == "" {
		f.Errors[fieldRemarks] = "Please specify the reason for the email account creation"
	} else if len(remarks) < MinRemarksLength || len(remarks) > MaxRemarksLength {
		f.Errors[fieldRemarks] = fmt.Sprintf("Reason must consist of %d to %d characters", MinRemarksLength, MaxRemarksLength)
	}
	data.Remarks = remarks

	return len(f.Errors) == 0, data
}

type createEmailAccountHandler struct {
	jwtValidityHours int
	FlashMessageAction
}

func (h *createEmailAccountHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var templateContext struct {
		Form *CreateEmailAccountForm
		baseTemplateContext
	}

	switch r.Method {
	case http.MethodPost:
		form := &CreateEmailAccountForm{
			RealName:       r.FormValue(fieldRealName),
			ContactEmail:   r.FormValue(fieldContactEmail),
			UserName:       r.FormValue(fieldUserName),
			FullNameAlias:  r.FormValue(fieldFullNameAlias),
			FullNameAlias2: r.FormValue(fieldFullNameAlias2),
			Role:           r.FormValue(fieldRole),
			Remarks:        r.FormValue(fieldRemarks),
			Payload:        &services.CreateEmailAccountPayload{},
		}

		templateContext.SetupContext(r)
		templateContext.Form = form
		if valid, data := form.Validate(); valid {
			data.RequesterEmailAddress = getEmails(r.Context())[0]
			data.ValidHours = h.jwtValidityHours
			var err error
			data.InitialPasswordUrl, err = h.buildInitialPasswordUrl(data.UserName, data.ContactEmail, r)
			if err != nil {
				log.Errorf("error creating initial password URL: %v", err)
				flash := &FlashMessage{
					Header:   "Could not create initial password URL",
					Severity: FlashError,
					Message:  "We could not create the initial password URL",
				}
				templateContext.Flashes = append(templateContext.Flashes, flash)
			} else {
				response, err := services.CreateEmailAccount(r, data)
				if err != nil {
					log.Errorf("error calling create email account API: %v", err)
					flash := &FlashMessage{
						Header:   "Could not process request",
						Severity: FlashError,
						Message:  "We could not process the create email account request",
					}
					templateContext.Flashes = append(templateContext.Flashes, flash)
				} else if response.Success {
					log.Infof(
						"Create email account with addresses %s for %s successful.",
						strings.Join(response.EmailAddresses, ", "), data.RealName)
					h.AddFlash(w, r, &FlashMessage{
						Header:   "Success",
						Severity: FlashSuccess,
						Message:  response.Message,
					})
					http.Redirect(w, r, UrlCreateEmailAccount, http.StatusFound)
				} else {
					log.Infof("Create email account for %s failed.", data.RealName)
					flash := &FlashMessage{
						Header:   "Create email account failed",
						Severity: FlashError,
						Message:  response.Message,
					}
					templateContext.Flashes = append(templateContext.Flashes, flash)
				}
			}
		}
		break
	case http.MethodGet:
		templateContext.SetupContext(r)
		templateContext.Form = &CreateEmailAccountForm{}
		break
	}
	if err := selfservice.RenderTemplate(w, "create-email-account.tmpl.html", http.StatusOK, templateContext); err != nil {
		internalError(w, err)
		return
	}
}

func (h *createEmailAccountHandler) buildInitialPasswordUrl(username string, email string, r *http.Request) (string, error) {
	var expiresAt time.Time
	expiresAt = time.Now().Add(time.Hour * time.Duration(h.jwtValidityHours))
	token := services.InitialRequestToken{
		EmailAddress: email,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expiresAt.Unix(),
			IssuedAt:  time.Now().Unix(),
			Issuer:    "CAcert selfservice",
			Subject:   username,
		},
	}
	jwtValue, err := token.Serialize()
	if err != nil {
		return "", err
	}
	initialPasswordUrl := url.URL{
		Scheme: "https",
		Host:   r.Host,
		Path:   fmt.Sprintf("%s%s", UrlInitialPassword, jwtValue),
	}
	return initialPasswordUrl.String(), nil
}
