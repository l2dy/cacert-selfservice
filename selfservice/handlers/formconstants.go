/*
   Copyright 2020 Jan Dittberner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this program except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package handlers

import "regexp"

// form field constraints
const (
	MinRealNameLength       = 5
	MaxRealNameLength       = 80
	MinUserNameLength       = 2
	MaxUserNameLength       = 40
	MinRemarksLength        = 5
	MaxRemarksLength        = 200
	MaxFullNameAliasLength  = 50
	MaxFullNameAlias2Length = 100
	MaxRoleLength           = 90
)

// form field names
const (
	fieldContactEmail   = "ContactEmail"
	fieldEmail          = "Email"
	fieldFullNameAlias  = "FullNameAlias"
	fieldFullNameAlias2 = "FullNameAlias2"
	fieldInitialToken   = "InitialToken"
	fieldNewPassword    = "NewPassword"
	fieldPasswordRepeat = "PasswordRepeat"
	fieldRealName       = "RealName"
	fieldRemarks        = "Remarks"
	fieldRole           = "Role"
	fieldUserName       = "UserName"
)

var userNameRegexp = regexp.MustCompile(`^[a-z]+$`)
var fullNameRegexp = regexp.MustCompile(`^[a-z]+[a-z.]+$`)
