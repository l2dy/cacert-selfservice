/*
   Copyright 2019, 2020 Jan Dittberner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this program except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package services

import "net/http"

type ResetPasswordPayload struct {
	EmailAddress          string `json:"email"`
	RequesterEmailAddress string `json:"requesterEmailAddress"`
	IsAdmin               bool   `json:"isAdmin"`
	NewPassword           string `json:"newPassword"`
	Remarks               string `json:"remarks"`
}

type ResetPasswordResponse struct {
	Success  bool   `json:"success"`
	Message  string `json:"message"`
	Username string `json:"username"`
}

func ResetPassword(r *http.Request, payload *ResetPasswordPayload) (*ResetPasswordResponse, error) {
	result := &ResetPasswordResponse{}
	if err := apiClientInstance.Call(r, "PUT", apiEmailPassword, payload, result); err != nil {
		return nil, err
	}
	return result, nil
}
