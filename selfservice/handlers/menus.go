/*
   Copyright 2019, 2020 Jan Dittberner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this program except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package handlers

import (
	"net/http"

	"github.com/gorilla/csrf"

	"git.cacert.org/cacert-selfservice/selfservice"
)

const (
	UrlAdminPasswordReset = "/admin-password-reset"
	UrlCreateEmailAccount = "/create-email-account"
	UrlDashboard          = "/"
	UrlErrorPage          = "/error-page"
	UrlPasswordReset      = "/password-reset"
	UrlStaff              = "/staff"
	UrlInitialPassword    = "/initial-password/"
	UrlFinished           = "/finished"
)

func getTopMenu(path string, emails []string, isAdmin bool) *selfservice.Menu {
	if emails != nil {
		return getAuthenticatedMenu(path, emails, isAdmin)
	}
	return getAnonymousMenu(path)
}

func getAnonymousMenu(path string) *selfservice.Menu {
	menu := selfservice.NewMenu()
	menu.AddItem(UrlDashboard, "Dashboard", "dashboard", path == UrlDashboard)
	menu.AddItem(UrlStaff, "Staff list", "staff", path == UrlStaff)
	return menu
}

func getAuthenticatedMenu(path string, emails []string, isAdmin bool) *selfservice.Menu {
	menu := selfservice.NewMenu()
	menu.AddItem(UrlDashboard, "Dashboard", "dashboard", path == UrlDashboard)
	menu.AddItem(UrlStaff, "Staff list", "staff", path == UrlStaff)
	// TODO: implement functionality for one-time password reset links
	if hasCAcertEmail(emails) {
		menu.AddItem(UrlPasswordReset, "Password reset", "password-reset", path == UrlPasswordReset)
	}
	if isAdmin {
		menu.AddItem(UrlAdminPasswordReset, "Admin password reset", "admin-password-reset", path == UrlAdminPasswordReset)
		menu.AddItem(UrlCreateEmailAccount, "Create new email account", "create-email-account", path == UrlCreateEmailAccount)
	}
	return menu
}

var csrfKey []byte

func SetupCsrfKey(csrfKeyBytes []byte) {
	csrfKey = csrfKeyBytes
}

func RegisterHandlers(adminEmails []string, jwtValidityHours int) {
	anonymousAuth := AnonymousAuth(adminEmails)
	userAuth := UserAuth(adminEmails)
	adminAuth := AdminAuth(adminEmails)

	http.Handle(UrlInitialPassword, authenticateRequest(userAuth, handleFlashes(&initialPasswordHandler{})))
	http.Handle(UrlDashboard, authenticateRequest(anonymousAuth, handleFlashes(&dashBoardHandler{})))
	http.Handle(UrlStaff, authenticateRequest(anonymousAuth, handleFlashes(&staffHandler{})))
	http.Handle(UrlFinished, authenticateRequest(userAuth, handleFlashes(&initialPasswordFinishedHandler{})))
	http.Handle(UrlPasswordReset,
		csrf.Protect(csrfKey)(authenticateRequest(userAuth, handleFlashes(&passwordResetHandler{}))))
	http.Handle(UrlAdminPasswordReset,
		csrf.Protect(csrfKey)(authenticateRequest(adminAuth, handleFlashes(&adminPasswordResetHandler{}))))
	http.Handle(UrlCreateEmailAccount,
		csrf.Protect(csrfKey)(authenticateRequest(adminAuth, handleFlashes(&createEmailAccountHandler{
			jwtValidityHours: jwtValidityHours,
		}))))
	http.Handle(UrlErrorPage, authenticateRequest(anonymousAuth, handleFlashes(&errorPageHandler{})))
}
