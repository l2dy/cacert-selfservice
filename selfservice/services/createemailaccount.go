/*
   Copyright 2020 Jan Dittberner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this program except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package services

import "net/http"

type CreateEmailAccountPayload struct {
	UserName              string `json:"username"`
	ContactEmail          string `json:"contactEmail"`
	RequesterEmailAddress string `json:"requestEmailAddress"`
	RealName              string `json:"realName"`
	FullNameAlias         string `json:"fullNameAlias"`
	FullNameAlias2        string `json:"fullNameAlias2"`
	Role                  string `json:"role"`
	Remarks               string `json:"remarks"`
	InitialPasswordUrl    string `json:"initial_password_url"`
	ValidHours            int    `json:"url_valid_hours"`
}

type CreateEmailAccountResponse struct {
	Success        bool     `json:"success"`
	Message        string   `json:"message"`
	Username       string   `json:"username"`
	EmailAddresses []string `json:"emailAddresses"`
}

func CreateEmailAccount(r *http.Request, payload *CreateEmailAccountPayload) (*CreateEmailAccountResponse, error) {
	result := &CreateEmailAccountResponse{}
	if err := apiClientInstance.Call(r, "POST", apiEmailAccount, payload, result); err != nil {
		return nil, err
	}
	return result, nil
}
