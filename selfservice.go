/*
   Copyright 2019, 2020 Jan Dittberner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this program except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package main

import (
	"context"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/tls"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"sync"
	"time"

	"github.com/google/uuid"
	gorillaHandlers "github.com/gorilla/handlers"
	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"

	"git.cacert.org/cacert-selfservice/selfservice"
	"git.cacert.org/cacert-selfservice/selfservice/handlers"
	"git.cacert.org/cacert-selfservice/selfservice/services"
)

var version = "undefined"
var build = "undefined"

func main() {
	log.SetFormatter(&log.TextFormatter{FullTimestamp: true})
	log.Infof("CAcert Community Self Service version %s, build %s", version, build)

	var configFile string
	flag.StringVar(
		&configFile, "config", "config.yaml", "Configuration file name")

	flag.Parse()

	var stopAll func()
	_, stopAll = context.WithCancel(context.Background())
	defer stopAll()

	config, err := readConfig(configFile)
	if err != nil {
		log.Errorf("%v", err)
		return
	}

	tlsConfig, err := setupTLSConfig(config)
	if err != nil {
		log.Errorf("%v", err)
		return
	}

	selfservice.SetTemplateConfig("templates/layout", "templates")
	if err := selfservice.LoadTemplates(); err != nil {
		log.Errorf("could not load templates: %v", err)
		return
	}

	server := &http.Server{
		Addr:              config.HttpsAddress,
		TLSConfig:         tlsConfig,
		IdleTimeout:       time.Second * 120,
		ReadHeaderTimeout: time.Second * 10,
		ReadTimeout:       time.Second * 20,
		WriteTimeout:      time.Second * 60,
	}

	handlers.RegisterHandlers(config.AdminEmails, config.JWTValidityHours)
	http.Handle("/static/", http.FileServer(selfservice.Assets))

	accessLog := &AccessLogWriter{
		LogFile: config.AccessLog,
	}

	server.Handler = gorillaHandlers.CompressHandler(
		gorillaHandlers.CombinedLoggingHandler(
			accessLog, http.DefaultServeMux))

	log.Infof("Launching application on https://%s/", server.Addr)

	if err := server.ListenAndServeTLS(config.ServerCert, config.ServerKey); err != nil {
		log.Errorf("ListenAndServerTLS failed: %v", err)
		return
	}
}

type Config struct {
	ClientCACertificates string   `yaml:"client_ca_certificates"`
	ServerCert           string   `yaml:"server_certificate"`
	ServerKey            string   `yaml:"server_key"`
	CookieSecret         string   `yaml:"cookie_secret"`
	CsrfKey              string   `yaml:"csrf_key"`
	BaseURL              string   `yaml:"base_url"`
	HttpsAddress         string   `yaml:"https_address"`
	APIPrivateKey        string   `yaml:"api_private_key"`
	APIEndpoint          string   `yaml:"api_endpoint_url"`
	APICACertificates    string   `yaml:"api_ca_certificates"`
	APIClientId          string   `yaml:"api_client_id"`
	AccessLog            string   `yaml:"access_log"`
	AdminEmails          []string `yaml:"admin_emails"`
	JWTPrivateKey        string   `yaml:"jwt_private_key"`
	JWTValidityHours     int      `yaml:"jwt_validity_hours"`
}

type AccessLogWriter struct {
	LogFile string
	mutex   *sync.Mutex
}

func (w *AccessLogWriter) Write(b []byte) (int, error) {
	if w.LogFile == "" {
		return os.Stdout.Write(b)
	}

	if w.mutex == nil {
		w.mutex = &sync.Mutex{}
	}

	w.mutex.Lock()
	f, err := os.OpenFile(w.LogFile, os.O_RDWR|os.O_APPEND|os.O_CREATE, 0644)
	if err != nil {
		return 0, err
	}
	defer func() {
		_ = f.Close()
		w.mutex.Unlock()
	}()
	return f.Write(b)
}

func readConfig(configFile string) (*Config, error) {
	source, err := ioutil.ReadFile(configFile)
	if err != nil {
		log.Errorf("opening configuration file failed: %v", err)
		generateExampleConfig()
		return nil, err
	}

	config := &Config{}
	if err := yaml.Unmarshal(source, config); err != nil {
		return nil, fmt.Errorf("loading configuration failed: %v", err)
	}

	if config.HttpsAddress == "" {
		config.HttpsAddress = "127.0.0.1:8443"
	}

	if config.APIPrivateKey == "" {
		return nil, fmt.Errorf("api_priv_key is not defined in %s", configFile)
	}

	if config.APIPrivateKey == "" {
		return nil, fmt.Errorf("api_priv_key is not defined in %s", configFile)
	}
	decodedBytes, _ := pem.Decode([]byte(config.APIPrivateKey))
	apiPrivateKey, err := x509.ParseECPrivateKey(decodedBytes.Bytes)
	if err != nil {
		return nil, fmt.Errorf("could not parse API private key: %v", err)
	}

	if config.APIEndpoint == "" {
		return nil, fmt.Errorf("api_endpoint_url is not defined")
	}
	err = services.ConfigureAPIClient(config.APIClientId, apiPrivateKey, config.APIEndpoint, config.APICACertificates)
	if err != nil {
		return nil, fmt.Errorf("could not configure API client: %v", err)
	}

	if config.JWTPrivateKey == "" {
		return nil, fmt.Errorf("jwt_priv_key is not defined in %s", configFile)
	}
	decodedBytes, _ = pem.Decode([]byte(config.JWTPrivateKey))
	jwtPrivateKey, err := x509.ParseECPrivateKey(decodedBytes.Bytes)
	if err != nil {
		return nil, fmt.Errorf("could not parse JWT private key: %v", err)
	}
	services.ConfigureJwtService(jwtPrivateKey)

	if config.JWTValidityHours < 1 {
		config.JWTValidityHours = 24
	}

	cookieSecret, err := base64.StdEncoding.DecodeString(config.CookieSecret)
	if err != nil {
		return nil, fmt.Errorf("decoding cookie secret failed: %v", err)
	}
	if len(cookieSecret) < 32 {
		return nil, fmt.Errorf("cookie secret is less than 32 bytes long")
	}
	csrfKey, err := base64.StdEncoding.DecodeString(config.CsrfKey)
	if err != nil {
		return nil, fmt.Errorf("decoding csrf key failed: %v", err)
	}
	if len(csrfKey) != 32 {
		return nil, fmt.Errorf("CSRF key must be exactly 32 bytes long but is %d bytes long", len(csrfKey))
	}

	handlers.SetupCsrfKey(csrfKey)
	handlers.SetupCookieStore(cookieSecret)
	log.Info("Read configuration")
	return config, nil
}

func generateExampleConfig() {
	apiPrivateKey, apiPublicKey, err := generateECKeyPair(false)
	if err != nil {
		log.Error(err)
		apiPrivateKey = "<pem encoded ECDSA private key>"
		apiPublicKey = "<pem encoded ECDSA public key>"
	}
	jwtPrivateKey, _, err := generateECKeyPair(true)
	if err != nil {
		log.Error(err)
		jwtPrivateKey = "<pem encoded ECDSA private key>"
		_ = "<pem encoded ECDSA public key>"
	}

	uuidString := uuid.New().String()

	config := &Config{
		ClientCACertificates: "cacert_roots.pem",
		ServerCert:           "server.crt.pem",
		ServerKey:            "server.key.pem",
		CookieSecret:         generateBase64EncodedRandomBytes(32),
		CsrfKey:              generateBase64EncodedRandomBytes(32),
		BaseURL:              "https://localhost:8443/",
		HttpsAddress:         "127.0.0.1:8443",
		APIPrivateKey:        apiPrivateKey,
		APIEndpoint:          "https://localhost:9443/",
		APICACertificates:    "api-ca.pem",
		APIClientId:          uuidString,
		JWTPrivateKey:        jwtPrivateKey,
		JWTValidityHours:     24,
		AccessLog:            "access.log",
		AdminEmails:          []string{"admin1@cacert.org", "admin2@cacert.org"},
	}
	configBytes, err := yaml.Marshal(config)
	if err != nil {
		log.Errorf("could not generate configuration data: %v", err)
		return
	}

	log.Infof("example data for config.yaml:\n\n---\n%s\n", configBytes)

	type ClientConfig struct {
		ClientId  string `yaml:"id"`
		PublicKey string `yaml:"key"`
	}

	type APIConfig struct {
		Clients []ClientConfig `yaml:"clients"`
	}

	apiConfig := &APIConfig{
		Clients: []ClientConfig{
			{ClientId: uuidString, PublicKey: apiPublicKey},
		},
	}
	apiConfigBytes, err := yaml.Marshal(apiConfig)
	if err != nil {
		log.Errorf("could not generate API configuration data: %v", err)
		return
	}

	log.Infof("example data for config.yaml on API server side:\n\n---\n%s\n", apiConfigBytes)
}

func generateBase64EncodedRandomBytes(count int) string {
	randomBytes := make([]byte, count)

	_, err := rand.Read(randomBytes)
	if err != nil {
		log.Errorf("could not read random bytes: %v", err)
		return fmt.Sprintf("<some random base64 encoded data of %d bytes>", count)
	}

	return base64.StdEncoding.EncodeToString(randomBytes)
}

func generateECKeyPair(privateOnly bool) (privatePEM string, publicPEM string, err error) {
	key, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	if err != nil {
		err = fmt.Errorf("could not generate private key: %v", err)
		return
	}
	privateBytes, err := x509.MarshalECPrivateKey(key)
	if err != nil {
		err = fmt.Errorf("could not marshal private key to DER: %v", err)
		return
	}
	privatePEM = string(
		pem.EncodeToMemory(&pem.Block{Type: "EC PRIVATE KEY", Bytes: privateBytes}))
	if privateOnly {
		return
	}

	publicBytes, err := x509.MarshalPKIXPublicKey(key.Public())
	if err != nil {
		err = fmt.Errorf("could not marshal public key: %v", err)
		return
	}
	publicPEM = string(pem.EncodeToMemory(&pem.Block{Type: "PUBLIC KEY", Bytes: publicBytes}))
	return
}

func setupTLSConfig(config *Config) (tlsConfig *tls.Config, err error) {
	// load CA certificates for client authentication
	caCert, err := ioutil.ReadFile(config.ClientCACertificates)
	if err != nil {
		err = fmt.Errorf("error reading client certificate CAs %v", err)
		return
	}
	caCertPool := x509.NewCertPool()
	if !caCertPool.AppendCertsFromPEM(caCert) {
		err = fmt.Errorf("could not initialize client CA certificate pool: %v", err)
		return
	}

	// setup HTTPS server
	tlsConfig = &tls.Config{
		ClientCAs:  caCertPool,
		ClientAuth: tls.VerifyClientCertIfGiven,
	}
	return
}
