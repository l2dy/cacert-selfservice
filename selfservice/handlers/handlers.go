/*
   Copyright 2019 Jan Dittberner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this program except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package handlers

import (
	"net/http"

	"git.cacert.org/cacert-selfservice/selfservice"
)

type baseTemplateContext struct {
	Flashes []interface{}
	TopMenu *selfservice.Menu
	Request *http.Request
	Emails  []string
}

func (c *baseTemplateContext) SetupContext(r *http.Request) {
	c.Flashes = getFlashes(r.Context())
	c.Emails = getEmails(r.Context())
	c.TopMenu = getTopMenu(r.URL.Path, c.Emails, isAdmin(r.Context()))
	c.Request = r
}
