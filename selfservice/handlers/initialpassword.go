/*
   Copyright 2020 Jan Dittberner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this program except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package handlers

import (
	"fmt"
	"net/http"
	"strings"

	log "github.com/sirupsen/logrus"

	"git.cacert.org/cacert-selfservice/selfservice"
	"git.cacert.org/cacert-selfservice/selfservice/services"
)

type InitialPasswordForm struct {
	NewPassword    string
	PasswordRepeat string
	Payload        *services.InitialPasswordPayload
	Errors         map[string]string
}

func (f *InitialPasswordForm) Validate() (bool, *services.InitialPasswordPayload) {
	f.Errors = make(map[string]string)

	data := f.Payload

	newPassword := strings.TrimSpace(f.NewPassword)
	newPasswordRepeat := strings.TrimSpace(f.NewPassword)

	if len(newPassword) < MinNewPasswordLength {
		f.Errors[fieldNewPassword] = fmt.Sprintf("Too short (need at least %d characters)", MinNewPasswordLength)
	} else if len(newPassword) > MaxPasswordLength {
		f.Errors[fieldNewPassword] = fmt.Sprintf("Too long (maximum of %d characters allowed)", MaxPasswordLength)
	}

	if newPassword != newPasswordRepeat {
		f.Errors[fieldPasswordRepeat] = "passwords do not match"
	}
	data.NewPassword = newPassword

	return len(f.Errors) == 0, data
}

type initialPasswordHandler struct {
	FlashMessageAction
}

func (h initialPasswordHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var templateContext struct {
		Form        *InitialPasswordForm
		TokenString string
		baseTemplateContext
	}

	switch r.Method {
	case http.MethodGet:
		tokenString := strings.TrimPrefix(r.RequestURI, UrlInitialPassword)
		token, err := services.BuildTokenFromString(tokenString)
		if err != nil {
			h.AddFlash(w, r, &FlashMessage{
				Header:   "Invalid request",
				Severity: FlashError,
				Message:  err.Error(),
			})
			http.Redirect(w, r, UrlDashboard, http.StatusFound)
			return
		}
		if err := token.CheckEmailMatch(getEmails(r.Context())); err != nil {
			log.Warnf("error checking email match: %v", err)
			h.AddFlash(w, r, &FlashMessage{
				Header:   "Invalid request",
				Severity: FlashError,
				Message:  err.Error(),
			})
			http.Redirect(w, r, UrlDashboard, http.StatusFound)
			return
		}
		log.Infof("authenticated with token for %s", token.EmailAddress)
		templateContext.SetupContext(r)
		templateContext.TokenString = tokenString
		templateContext.Form = &InitialPasswordForm{Errors: make(map[string]string, 0)}

		break
	case http.MethodPost:
		tokenString := strings.TrimSpace(r.FormValue(fieldInitialToken))
		token, err := services.BuildTokenFromString(tokenString)
		if err != nil {
			h.AddFlash(w, r, &FlashMessage{
				Header:   "Invalid request",
				Severity: FlashError,
				Message:  err.Error(),
			})
			http.Redirect(w, r, UrlDashboard, http.StatusFound)
			return
		}
		if err := token.CheckEmailMatch(getEmails(r.Context())); err != nil {
			log.Warnf("error checking email match: %v", err)
			h.AddFlash(w, r, &FlashMessage{
				Header:   "Invalid request",
				Severity: FlashError,
				Message:  err.Error(),
			})
			http.Redirect(w, r, UrlDashboard, http.StatusFound)
			return
		}
		log.Infof("authenticated with token for %s", token.EmailAddress)

		form := &InitialPasswordForm{
			NewPassword:    r.FormValue(fieldNewPassword),
			PasswordRepeat: r.FormValue(fieldPasswordRepeat),
			Payload: &services.InitialPasswordPayload{
				Username:     token.Subject,
				ContactEmail: token.EmailAddress,
			},
		}
		valid, data := form.Validate()

		templateContext.SetupContext(r)
		templateContext.Form = form
		templateContext.TokenString = tokenString

		if valid {
			response, err := services.SetInitialPassword(r, data)
			if err != nil {
				log.Errorf("error calling set initial password API: %v", err)
				h.AddFlash(w, r, &FlashMessage{
					Header:   "Could not process request",
					Severity: FlashError,
					Message:  "We could not process your initial password request.",
				})
			} else if response.Success {
				log.Infof(
					"Password reset for email addresses %s successful.", strings.Join(response.EmailAddresses, ", "))
				h.AddFlash(w, r, &FlashMessage{
					Header:   "Success",
					Severity: FlashSuccess,
					Message:  response.Message,
				})
				http.Redirect(w, r, UrlFinished, http.StatusFound)
			} else {
				log.Infof("Setting initial password for user %s failed.", data.Username)
				h.AddFlash(w, r, &FlashMessage{
					Header:   "Could not set initial password",
					Severity: FlashError,
					Message:  response.Message,
				})
			}
		}
		break
	default:
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}

	if err := selfservice.RenderTemplate(w, "initial-password.tmpl.html", http.StatusOK, templateContext); err != nil {
		internalError(w, err)
		return
	}
}

type initialPasswordFinishedHandler struct {
	FlashMessageAction
}

func (f *initialPasswordFinishedHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var templateContext struct {
		baseTemplateContext
	}

	templateContext.SetupContext(r)

	if r.Method != http.MethodGet {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}
	if err := selfservice.RenderTemplate(w, "initial-password-finished.tmpl.html", http.StatusOK, templateContext); err != nil {
		internalError(w, err)
	}
}
