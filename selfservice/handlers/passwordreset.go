/*
   Copyright 2019-2021 Jan Dittberner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this program except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package handlers

import (
	"fmt"
	"net/http"
	"strings"

	log "github.com/sirupsen/logrus"

	"git.cacert.org/cacert-selfservice/selfservice"
	"git.cacert.org/cacert-selfservice/selfservice/services"
)

const (
	MinNewPasswordLength = 10
	MaxPasswordLength    = 2048 // restrict maximum length to avoid denial of service due to extremely long hashing
)

type PasswordResetForm struct {
	Email          string
	NewPassword    string
	PasswordRepeat string
	Payload        *services.ResetPasswordPayload
	Errors         map[string]string
}

func (f *PasswordResetForm) Validate(emails []string) (bool, *services.ResetPasswordPayload) {
	f.Errors = make(map[string]string)

	data := f.Payload

	data.IsAdmin = false

	emailAddress := strings.TrimSpace(f.Email)
	for _, e := range emails {
		if e == emailAddress && strings.HasSuffix(emailAddress, "@cacert.org") {
			data.EmailAddress = emailAddress
		}
	}
	if data.EmailAddress == "" {
		f.Errors["Email"] = "You need to choose one of your own cacert.org email addresses"
	}

	newPassword := strings.TrimSpace(f.NewPassword)
	newPasswordRepeat := strings.TrimSpace(f.PasswordRepeat)

	if len(newPassword) < MinNewPasswordLength {
		f.Errors["NewPassword"] = fmt.Sprintf("Too short (need at least %d characters)", MinNewPasswordLength)
	} else if len(newPassword) > MaxPasswordLength {
		f.Errors["NewPassword"] = fmt.Sprintf("Too long (maximum of %d characters allowed)", MaxPasswordLength)
	}

	if newPassword != newPasswordRepeat {
		f.Errors["PasswordRepeat"] = "passwords do not match"
	}
	data.NewPassword = newPassword

	return len(f.Errors) == 0, data
}

type passwordResetHandler struct {
	FlashMessageAction
}

func (h *passwordResetHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var templateContext struct {
		Form *PasswordResetForm
		baseTemplateContext
	}

	switch r.Method {
	case http.MethodPost:
		form := &PasswordResetForm{
			Email:          r.FormValue("Email"),
			NewPassword:    r.FormValue("NewPassword"),
			PasswordRepeat: r.FormValue("PasswordRepeat"),
			Payload:        &services.ResetPasswordPayload{},
		}

		if valid, data := form.Validate(getEmails(r.Context())); !valid {
			templateContext.SetupContext(r)
			templateContext.Form = form
		} else {
			data.RequesterEmailAddress = getEmails(r.Context())[0]
			response, err := services.ResetPassword(r, data)
			if err != nil {
				log.Errorf("error calling reset password API: %v", err)
				h.AddFlash(w, r, &FlashMessage{
					Header:   "Could not process request",
					Severity: FlashError,
					Message:  "We could not process your password reset request. Try again later.",
				})
			} else if response.Success {
				log.Infof(
					"Password reset for user %s with email %s successful.",
					response.Username, data.EmailAddress)
				h.AddFlash(w, r, &FlashMessage{
					Header:   "Success",
					Severity: FlashSuccess,
					Message:  response.Message,
				})
			} else {
				log.Infof(
					"Password reset for email %s failed.", data.EmailAddress)
				h.AddFlash(w, r, &FlashMessage{
					Severity: FlashError,
					Message:  response.Message,
				})
			}
			http.Redirect(w, r, "/password-reset", http.StatusFound)
			return
		}

		break
	case http.MethodGet:
		templateContext.SetupContext(r)
		templateContext.Form = &PasswordResetForm{
			Email: templateContext.Emails[0],
		}

		break
	default:
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}
	if err := selfservice.RenderTemplate(w, "password-reset.tmpl.html", http.StatusOK, templateContext); err != nil {
		internalError(w, err)
		return
	}
}
